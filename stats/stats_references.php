#!/usr/bin/php
<?PHP

error_reporting (E_ERROR | E_WARNING | E_PARSE) ;

// S=string; T=time; C=coordinate; no letter=item

// Wikipedia item list: http://208.80.153.172/api?q=claim[31:52]
$wikipedias = array ( 328,8447,8449,10000,11913,11918,11920,11921,12237,14380,17985,30239,38288,45041,48183,48952,53464,58172,58209,58215,58251,58255,58679,58781,60786,60799,60819,60856,79633,79636,155214,169514,175482,177837,181163,190551,191168,191769,192582,199693,199698,199700,199864,199913,200060,200180,200183,200386,202472,203488,206855,207260,208533,221444,225594,226150,427715,511754,547271,565074,571001,588620,595628,714826,718394,722040,722243,728945,766705,824297,837615,841208,842341,844491,845993,846630,846871,848046,848525,848974,856881,874555,875631,877583,877685,925661,928808,940309,950058,966609,1034940,1047829,1047851,1055841,1058430,1066461,1067878,1071918,1110233,1116066,1132977,1147071,1148240,1154741,1154766,1178461,1190962,1211233,1249553,1287192,1291627,1377618,1378484,1444686,1551807,1574617,1585232,1648786,1754193,1961887,1968379,1975217,2029239,2073394,2081526,2091593,2111591,2328409,2349453,2374285,2402143,2587255,2602203,2732019,2742472,2744155,2913253,2983979,2998037,3025527,3025736,3026819,3046353,3111179,3112631,3123304,3180091,3180306,3181422,3181928,3239456,3311132,3432470,3477935,3486726,3568035,3568038,3568039,3568040,3568041,3568042,3568043,3568044,3568045,3568046,3568048,3568049,3568051,3568053,3568054,3568056,3568059,3568060,3568061,3568062,3568063,3568065,3568066,3568069,3696028,3753095,3756269,3756562,3757068,3807895,3826575,3913095,3913160,3944107,3957795,4077512,4097773,4107346,4115441,4115463,4210231,4296423,4372058,4614845,4783991,4925786,5652665,6112922,6125437,6167360,6587084,7102897,8042979,8075204,8558731,8558960,8559119,8559737,8560590,8561147,8561277,8561332,8561415,8561491,8561552,8561582,8561662,8561870,8562097,8562272,8562481,8562502,8562529,8562927,8563136,8563393,8563635,8563685,8563863,8564352,8565447,8565463,8565476,8565518,8565742,8565801,8566298,8566311,8566347,8566503,8568791,8569757,8569951,8570048,8570353,8570425,8570791,8571143,8571427,8571487,8571809,8571840,8571954,8572132,8572199,8575385,8575467,8575674,8575782,8575885,8575930,8576190,8576237,8577029,8582589,8669146,8937989,12265494,13230970,13231253,13358221 ) ;

$out = array (
	'statements_per_item' => array() ,
	'items_with_referenced_statements' => 0 ,
	'total_items' => 0 ,
	'references_per_statement_type' => array() , 
	'wp_reference_statements' => array() ,
	'non_wp_reference_statements' => array() ,
	'labels_per_item' => array() ,
	'links_per_item' => array() ,
	'descs_per_item' => array() ,
	'rank_per_statement' => array( 'deprecated' => 0 , 'normal' => 0 , 'preferred' => 0 ) ,
) ;

//$ranks = array('deprecated','normal','preferred') ;

$title = '' ;
$ns = '' ;
$last_text = '' ;
while ( !feof ( STDIN ) ) {
	$line = fgets(STDIN);
	$line = trim ( preg_replace ( '/,\s*$/' , '' , $line ) ) ;
	$json = json_decode ( $line ) ;
	if ( $json->type == 'property' ) continue ;
	
	$last_text = '' ;
	if ( $ns != 0 ) continue ; // Items only
	
	
	if ( isset ( $json->labels ) ) {
		$n = 0 ;
		foreach ( $json->labels AS $z ) $n++ ;
		$out['labels_per_item'][$n]++ ;
	} else {
		$out['labels_per_item'][0]++ ;
	}

	if ( isset ( $json->descriptions )) {
		$n = 0 ;
		foreach ( $json->descriptions AS $dummy ) $n++ ;
		$out['descs_per_item'][$n]++ ;
	} else {
		$out['descs_per_item'][0]++ ;
	}
	
	if ( isset ( $json->sitelinks ) ) {
		$n = 0 ;
		foreach ( $json->sitelinks AS $dummy ) $n++ ;
		$out['links_per_item'][$n]++ ;
	} else {
		$out['links_per_item'][0]++ ;
	}
	
	$out['total_items']++ ;
	if ( isset ( $json->claims ) ) {
		$has_a_reference = false ;
		$statement_count = 0 ;

		foreach ( $json->claims AS $p => $vl ) {
			foreach ( $vl AS $dummy => $v ) {
			
				if ( $v->type != 'statement' ) continue ;
			
				$statement_count++ ;
		
				// Rank
				if ( isset ( $v->rank ) ) {
					$out['rank_per_statement'][$v->rank]++ ;
				}

				// Claim type
				$type = 'unknown' ;
				if ( isset($v->mainsnak) ) {
					if ( $v->mainsnak->snaktype == 'novalue' ) $type = 'novalue' ;
					else if ( $v->mainsnak->snaktype == 'somevalue' ) $type = 'somevalue' ;
					else if ( isset($v->mainsnak->datavalue) ) {
						$m = $v->mainsnak->datavalue ;
						$t = $m->type ;
						if ( $t == 'wikibase-entityid' ) {
							$type = 'itemlink' ;
						} else if ( $t == 'bad' ) {
							$type = 'bad' ;
						} else if ( $t == 'string' ) {
							$type = 'string' ;
						} else if ( $t == 'time' ) {
							$type = 'time' ;
						} else if ( $t == 'globecoordinate' ) {
							$type = 'globecoordinate' ;
						} else if ( $t == 'quantity' ) {
							$type = 'quantity' ;
						}
					}
				}
				
				// References
				if ( isset ( $v->references ) and count($v->references) > 0 ) {
					$out['references_per_statement_type'][$type]++ ;

					$non_wp_ref = false ;                                	
					foreach ( $v->references AS $k2 => $v2 ) {
						if ( !isset($v2->snaks) ) continue ;
						$has_a_reference = true ;
						
						foreach ( $v2->snaks AS $refprop => $reflist ) {
						
							foreach ( $reflist AS $v3 ) {
//                                			$ref_prop = $v3[1] ;
								$ref_type = isset($v3->datavalue) ? $v3->datavalue->type : 'unknown' ;
								if ( $ref_type == 'wikibase-entityid' ) {
									$target = $v3->datavalue->value->{'numeric-id'} ;
									if ( !in_array ( $target , $wikipedias ) ) $non_wp_ref = true ;
								}
								$out['references_properties'][$ref_type]++ ;
							}
						
						}
					}
					if ( $non_wp_ref ) $out['non_wp_reference_statements'][$type]++ ;
					else $out['wp_reference_statements'][$type]++ ;
					
				} else {
					$out['references_per_statement_type'][$type]++ ;
				}
			}
		}
		 
		$out['statements_per_item'][$statement_count]++ ;

		if ( $has_a_reference ) $out['items_with_referenced_statements']++ ;
		
	} else {
		$out['statements_per_item'][0]++ ;
	}
}

//print_r ( $out ) ;
print json_encode ( $out ) ;

?>
