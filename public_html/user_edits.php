<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','1500M');
set_time_limit ( 60 * 10 ) ; // Seconds

require_once ( './php/common.php' ) ;

function prettyTS ( $ts ) {
		return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).'&nbsp;'.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2) ;
	}

$format = get_request ( 'format' , 'html' ) ;
$manual = trim ( get_request ( 'manual' , '' ) ) ;
$sparql = trim ( get_request ( 'sparql' , '' ) ) ; # 'SELECT DISTINCT ?q { ?q (p:P569|p:P570) ?statement . ?statement prov:wasDerivedFrom/<http://www.wikidata.org/prop/reference/P4025> [] }' ; # birth or death with a reference with P4025 statement
$user = trim ( get_request ( 'user' , '' ) ) ; # 'Reinheitsgebot' ;
$pattern = trim ( get_request ( 'pattern' , '' ) ) ; #'wbcreateclaim.*\b(P570|P569)\b.*Mix\'n\'match:References$' ;
$start = trim ( get_request ( 'start' , '' ) ) ;
$end = trim ( get_request ( 'end' , '' ) ) ;

print get_common_header ( '' , 'User edits' ) ;

print "
<p>
Shows edits for a user/bot on Wikidata, for a set of items, with a specific revision comment pattern. Example:<br/>
<a style='font-size:9pt' href='?sparql=SELECT+DISTINCT+%3Fq+%7B+%0D%0A%3Fq+%28p%3AP569%7Cp%3AP570%29+%3Fstatement+.+%0D%0A%3Fstatement+prov%3AwasDerivedFrom%2F%3Chttp%3A%2F%2Fwww.wikidata.org%2Fprop%2Freference%2FP4025%3E+%5B%5D+%0D%0A%7D&pattern=wbcreateclaim.*%5Cb%28P570%7CP569%29%5Cb.*Mix%27n%27match%3AReferences%24&user=Reinheitsgebot&doit=Do+it'>
Edits by User:Reinheitsgebot, with a pattern of creating a birth or death date as part of \"Mix'n'match:References\", for items where the birth or death date has a reference with a Pinakothek (P4025) statement</a>
</p>
<form method='get' class='form'>
<!--
<p>Manual item list <small>(can not be used together with SPARQL)</small>
<textarea rows=4 style='width:100%' name='manual'>$manual</textarea>
</p>
 <small>(can not be used together with manual item list)</small>
-->
<p><a href='https://query.wikidata.org' target='_blank'>SPARQL</a> <small>(items need to be returned as ?q)</small>
<textarea rows=3 style='width:100%' name='sparql'>" . htmlentities($sparql) . "</textarea>
</p>
<p>
<div>
Pattern <small>(regular expression)</small>
<input type='text' name='pattern' style='width:100%' value='" . escape_attribute($pattern) . "' />
</div>
<div style='font-size:8pt;'>Typical actions include:
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbcreateclaim-create</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbcreateredirect</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbeditentity-create</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbeditentity-override</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbeditentity-update</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbmergeitems-from</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbmergeitems-to</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbremoveclaims-remove</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetaliases-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetaliases-remove</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetaliases-set</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetaliases-update</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetclaim-create</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetclaim-update</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetclaimvalue</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetdescription-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetdescription-remove</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetdescription-set</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetlabel-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetlabeldescriptionaliases</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetlabel-remove</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetlabel-set</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetqualifier-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetqualifier-update</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetreference-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetsitelink-add</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetsitelink-remove</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetsitelink-set</span>
<span style='padding-right:5px;background-color:#DDD;border:1px solid white;'>wbsetsitelink-set-both</span>
</div>

</p>
<p>User
<input type='text' name='user' placeholder='username, no prefix' style='width:100%' value='" . escape_attribute($user) . "' />
<br/><small>any user or bot, e.g. QuickStatementsBot or Reinheitsgebot</small>
</p>
<p>
<div>Date range</div>
<div><input type='text' name='start' style='width:50%' value='" . escape_attribute($start) . "' placeholder='Start date (YYYYMMDDHHMMSS, shorter allowed)' /></div>
<div><input type='text' name='end' style='width:50%' value='" . escape_attribute($end) . "' placeholder='End date (YYYYMMDDHHMMSS, shorter allowed)' /></div>
</p>
<p>
<input type='submit' class='btn btn-primary' name='doit' value='Do it' />
<label><input type='radio' name='format' value='html'" . ($format=='html'?' checked':'') . "> HTML</label>
<label><input type='radio' name='format' value='text'" . ($format=='text'?' checked':'') . "> Text</label>
</p>
</form>
" ;

if ( !isset($_REQUEST['doit']) ) exit ( 0 ) ;

// Get potentially affected items
$items = [] ;

if ( $manual != '' ) {
	// TODO
} else if ( isset($sparql) and $sparql != '' ) {
	$items = getSPARQLitems ( $sparql ) ;
	foreach ( $items AS $k => $v ) $items[$k] = "Q$v" ;
	if ( count($items) == 0 ) die ( "No items\n" ) ;
}

function getWikidataItemLabel ( &$db , $q , $languages = ['en','de','nl','fr','es','it','zh'] ) {
	$ret = $q ; // Default label: item ID
	$q = $db->real_escape_string ( $q ) ;
	$lang2label = [] ;
	$sql = "SELECT * FROM wb_terms WHERE term_full_entity_id='{$q}' AND term_type='label'" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $lang2label[$o->term_language] = $o->term_text ;
	foreach ( $languages AS $lang ) {
		if ( isset($lang2label[$lang]) ) return $lang2label[$lang] ;
	}
	return $ret ;
}


// TESTING, DOES NOT WORK YET
function getUndoButton ( $q , $rev_parent_id , $rev_id ) {
	$ret = "
<form id='undo' name='undo' method='post' target='_blank' action='https://www.wikidata.org/w/index.php?title=$q&amp;action=submit&amp;undo=$rev_id&amp;undoafter=$rev_parent_id' enctype='multipart/form-data'>
<input type='hidden' name='wpSummary' value='Via https://tools.wmflabs.org/wikidata-todo/user_edits.php' maxlength='200' id='ooui-1' />
<button type='submit' name='wpSave' value='Undo' class='brn btn-sm btn-danger'>Undo</button>
<input type='hidden' value='$rev_id' name='wpUndidRevision'/>
</form>
" ;
//<input type='hidden' value='91deade5e00d18e36bb9ca9fb7d749f25a0eb19d+\' name='wpEditToken'/>
//<input type='hidden' value='595016661' name='wpBaseRev'/>
	return $ret ;
}

// Get edits
$user_id = '' ;
$db = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "SELECT user_id FROM user WHERE user_name='" . $db->real_escape_string($user) . "'" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()) $user_id = $o->user_id ;
if ( $user_id == '' ) die ( "Unknown user $user\n" ) ;

if ( $format == 'text' ) {
	print "<textarea style='width:100%;font-family:Courier;font-size:6pt' rows=20>" ;
} else {
	print "<h3>Results</h3>" ;
	print "<table class='table table-sm table-striped'>" ;
	print "<tbody>" ;
}

$cnt = 0 ;
if ( count($items) == 0 ) $items_meta = [ [ '' ] ] ;
else $items_meta = array_chunk ( $items , 10000 ) ;
$items = [] ;
foreach ( $items_meta AS $items ) {
	$sql = "SELECT rev_id,rev_comment,rev_parent_id,page_title,rev_timestamp from page,revision_userindex where page_id=rev_page AND page_namespace=0 AND rev_user=$user_id" ;
	if ( count($items)>1 or $items[0] != '' ) $sql .= " AND page_title IN ('" . implode("','",$items) . "')" ;
	if ( $start != '' ) $sql .= " AND rev_timestamp>='" . ($start*1) . "'" ;
	if ( $end != '' ) $sql .= " AND rev_timestamp<='" . ($end*1) . "'" ;
	$sql .= " ORDER BY rev_page" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) {
		if ( !preg_match ( '/' . str_replace('/','\\/',$pattern) . '/' , $o->rev_comment ) ) continue ;
		$cnt++ ;
		$url_undo = "https://www.wikidata.org/w/index.php?title={$o->page_title}&action=edit&undoafter={$o->rev_parent_id}&undo={$o->rev_id}" ;
		$url_undo_api = "https://www.wikidata.org/w/api.php?action=edit&title={$o->page_title}&undo={$o->rev_id}&token=" ;
		$url_diff = "https://www.wikidata.org/w/index.php?title={$o->page_title}&diff={$o->rev_id}&oldid={$o->rev_parent_id}" ;
		$q = $o->page_title ;
		$undo_button = '' ;
		if ( isset($_REQUEST['test']) ) {
			$undo_button = '<br/>' . getUndoButton ( $q , $o->rev_parent_id , $o->rev_id ) ;
		} 
		$label = getWikidataItemLabel ( $db , $q ) ;

		if ( $format == 'text' ) {
			print prettyTS ( $o->rev_timestamp ) . "\t{$q}\t" . htmlentities(str_replace("\t"," ",$label)) . "\t" . htmlentities(str_replace("\t"," ",$o->rev_comment)) . "\t{$o->rev_id}\t{$o->rev_parent_id}\n" ;
		} else {
			$comment = htmlentities($o->rev_comment) ;
			$comment = preg_replace ( '/\[\[(Q\d+)\]\]/' , '[[<a href="https://www.wikidata.org/wiki/$1" class="wikidata" target="_blank">$1</a>]]' , $comment ) ;
			$comment = preg_replace ( '/\[\[(Property:P\d+)\]\]/' , '[[<a href="https://www.wikidata.org/wiki/$1" class="wikidata" target="_blank">$1</a>]]' , $comment ) ;
			print "<tr>" ;
			print "<td style='font-family:courier;font-size:9pt;text-align:right;' nowrap>" . number_format ( $cnt ) . "</td>" ;
			print "<td style='font-family:courier;font-size:9pt;' nowrap>" . prettyTS ( $o->rev_timestamp ) . "</td>" ;
			print "<td><a href='https://www.wikidata.org/wiki/$q' target='_blank'>{$label}</a> <small>[$q]</small></td>" ;
			print "<td><small>" . $comment . "</small></td>" ;
			print "<td><a href='$url_diff' target='_blank'>diff</a></td>" ;
			print "<td><a href='$url_undo' target='_blank'>undo</a>$undo_button</td>" ;
			print "</tr>" ;
		}

	}
}

if ( $format == 'text' ) {
	print "</textarea>" ;
	print "<p>Columns (tab-separated): timestamp, item, label, edit comment, revision ID, parent revision ID</p>" ;
} else {
	print "</tbody></table>" ;
}

print "Done." ;

print get_common_footer() ;

?>