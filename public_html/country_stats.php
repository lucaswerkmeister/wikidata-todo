<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
require_once ( "php/common.php" ) ;

print get_common_header ( '' , 'Country stats' ) ;

$dbu = openToolDB ( 'monitor_p' ) ;

$data = array() ;
$heading = array() ;
$languages = array() ;
$sql = "SELECT * FROM country ORDER BY day" ;
$result = getSQL ( $dbu , $sql ) ;
while($o = $result->fetch_object()){
	$heading[$o->country_code][$o->key] = $o->key ;
	$data[$o->country_code][$o->day][$o->key] = array ( $o->items , $o->query ) ;
	$languages[$o->main_lang] = $o->main_lang ;
}



$fractions = array (
	'citizens_male' => 'citizens' ,
	'citizens_female' => 'citizens' ,
	'citizens_with_image' => 'citizens' ,
	'citizens_with_image' => 'citizens' ,
	'by_admin_only' => 'by_admin_or_coord' ,
	'by_coord_only' => 'by_admin_or_coord' ,
	'by_coord' => 'by_admin_or_coord' ,
	'by_admin' => 'by_admin_or_coord' ,
	'by_admin_and_coord' => 'by_admin_or_coord' ,
	'by_admin_or_coord_with_images' => 'by_admin_or_coord'
) ;

foreach ( $languages AS $lang ) {
	$fractions['citizens_no_'.$lang.'.wp'] = 'citizens' ;
	$fractions['by_admin_or_coord_no_'.$lang.'.wp'] = 'by_admin_or_coord' ;
}


foreach ( $data AS $country => $l1 ) {
	$head = $heading[$country] ;
	sort ( $head ) ;
//	print "<pre>" ; print_r ( $head ) ; print "</pre>" ;
	print "<h2>$country</h2>" ;
	print "<table class='table table-striped table-compact'>" ;
	print "<thead><tr><th>Day</th>" ;
	foreach ( $head AS $h ) print "<th style='text-align:right'>" . str_replace('_',' ',$h) . "</th>" ;
	print "</tr></thead><tbody>" ;
	
	$days = array_keys ( $l1 ) ;
	sort ( $days ) ;
	$last = array() ;
	foreach ( $days AS $d ) {
		print "<tr>" ;
		print "<th>$d</th>" ;
		foreach ( $head AS $h ) {
			print "<td style='font-family:Courier;text-align:right'>" ;
			if ( isset ( $l1[$d][$h] ) ) {
				if ( $l1[$d][$h][1] == '' ) print $l1[$d][$h][0] ;
				else print "<a target='_blank' title='Run this query now!' href='/autolist/?wdq=".urlencode($l1[$d][$h][1])."&run=Run'>" . $l1[$d][$h][0] . "</a>" ;
				
				print "<br/>" ;
				if ( isset($last[$h]) ) {
					$diff = $l1[$d][$h][0] - $last[$h][0] ;
					if ( $diff < 0 ) print "<span style='color:red'>$diff</span>" ;
					if ( $diff > 0 ) print "<span style='color:green'>+$diff</span>" ;
				}
				
				if ( isset($fractions[$h]) and isset($l1[$d][$fractions[$h]]) ) {
					printf ( "<br/>%2.1f%%" , 100*$l1[$d][$h][0]/$l1[$d][$fractions[$h]][0] ) ;
				}
			} else print "&mdash;" ;
			print "</td>" ;
		}
		print "</tr>" ;
		$last = $l1[$d] ;
	}
	
	print "</tbody></table>" ;
}
// https://tools.wmflabs.org/autolist/?language=en&project=wikipedia&category=&depth=12&wdq=claim%5B1234%5D&mode=undefined&statementlist=&run=Run&label_contains=&label_contains_not=&chunk_size=10000

print "<script>
$(document).ready ( function () {
	$('div.span12').removeClass('span12');
	$('#main_content').removeClass('container').addClass('container-fluid');
} ) ;
</script>" ;

print get_common_footer() ;

?>