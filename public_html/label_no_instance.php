<?PHP

require_once ( 'php/common.php' ) ;

print get_common_header ( '' , 'Wikidatas item with label pattern but without "instance of"' ) ;

$lang = get_request ( 'lang' , '' ) ;
$pattern = get_request ( 'pattern' , '' ) ;

print "<div class='lead'>Enter a language code and a MySQL LIKE pattern (e.g. \"Burg %\"), and get a list of items with matching labels but no \"instance of\".</div>" ;
print "<form method='get' action='?'><input type='text' name='lang' value='$lang' placeholder='lang'/><input type='text' name='pattern' value='$pattern' placeholder='Pattern' /><input type='submit' name='run' class='btn btn-primary' /></form>" ;

if ( isset ( $_REQUEST['run'] ) ) {

print "<hr/>" ;

	$db = openDB ( 'wikidata' , '' ) ;
	$lang = $db->real_escape_string ( $lang ) ;
	$pattern = $db->real_escape_string ( $pattern ) ;

	$sql = 'select distinct page_title from page,wb_terms where page_namespace=0 and not exists ( select * from pagelinks where page_id=pl_from and pl_namespace=120 and (pl_title="P31" OR pl_title="P279") limit 1)' ;
	$sql .= " and term_language='$lang' and term_type='label' and term_entity_type='item' and term_full_entity_id=page_title and term_text like '$pattern'" ;

	print "<h2>Results</h2><form target='_blank' method='post' action='./autolist2.php'><textarea name='manual_list' rows='10'>" ;
	$result = getSQL ( $db , $sql ) ;
	$cnt = 0 ;
	while($r = $result->fetch_object()){
		print $r->page_title . "\n" ;
		$cnt++ ;
	}
	print "</textarea><br/><input type='submit' class='btn btn-primary' name='run' value='Autolist2'></form>" ;
	print "<div>$cnt results total.</div>" ;
}

print get_common_footer() ;

?>