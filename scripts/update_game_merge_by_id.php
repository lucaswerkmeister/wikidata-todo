#!/usr/bin/php
<?php

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;

$tfc = new ToolforgeCommon ( 'wikidata-todo' ) ;

$props_blacklist = [ 'P709' , 'P638' , 'P484' , 'P594' , 'P705' , 'P1566' , 'P2249' ] ;

$props = [] ;
$sparql = 'SELECT ?property WHERE { ?property wikibase:propertyType wikibase:ExternalId ; wdt:P2302 wd:Q21502410 }' ; # Properties with external-id and "distinct value constraint"
$props = $tfc->getSPARQLitems ( $sparql ) ;

$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;
$dbt = $tfc->openDBtool ( 'wikidata_merge_by_id_p' ) ;
foreach ( $props AS $prop ) {
	if ( in_array ( $prop , $props_blacklist) ) continue ;
	$prop_num = preg_replace ( '/\D/' , '' , $prop ) * 1 ;
	$sql = "UPDATE candidates SET `tag`=1 WHERE prop={$prop_num}" ;
	$tfc->getSQL ( $dbt , $sql ) ;
	$sparql = "SELECT ?q1 ?q2 ?value { ?q1 wdt:{$prop} ?value . ?q2 wdt:{$prop} ?value . FILTER ( ?q1 != ?q2 ) }" ;
	$j = $tfc->getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		$q1 = preg_replace ( '/\D/' , '' , $tfc->parseItemFromURL ( $b->q1->value ) ) * 1 ;
		$q2 = preg_replace ( '/\D/' , '' , $tfc->parseItemFromURL ( $b->q2->value ) ) * 1 ;
		if ( $q1 > $q2 ) list ( $q1 , $q2 ) = [ $q2 , $q1 ] ;

		# Check for 'different from'
		$sql = "SELECT * FROM pagelinks WHERE pl_namespace=120 AND pl_title='P1889' AND pl_from IN (select page_id FROM page WHERE page_namespace=0 AND page_title IN ('Q{$q1}','Q{$q2}'))" ;
		$result = $tfc->getSQL ( $db , $sql ) ;
		if($o = $result->fetch_object()) continue ;

		$sql = "INSERT INTO candidates (q1,q2,prop,`value`,random,tag) VALUES ({$q1},{$q2},{$prop_num},'".$dbt->real_escape_string(substr($b->value->value,0,32))."',rand(),0) ON DUPLICATE KEY UPDATE `tag`=0" ;
		$tfc->getSQL ( $dbt , $sql ) ;
	}
	$sql = "DELETE FROM candidates WHERE prop={$prop_num} AND tag=1 AND `done`='OPEN'" ;
	$tfc->getSQL ( $dbt , $sql ) ;
}

?>